<?php
    //set output variables. These will be echo'd in the output section
    //they appear here in case we output diagnostics
    $css_output = '';
    $html_output = '';
    $tab = '&nbsp;&nbsp;&nbsp;&nbsp;';
    if ($_GET['number_of_slides']) {
        //grab basic variables from get
        $slider_name = $_GET['slider_name'];
        //make a version of the name that is variable friendly
        $slider_id = strtolower(str_replace(' ','_',$slider_name)) . '_slider';
        $slide_amount = $_GET['number_of_slides'];
        $slide_time = $_GET['slide_time'];
        $transition_time = $_GET['transition_time'];
        //need the frame time to calculate the percentage of time that
        //slides and transitions take per image
        $frame_time = $slide_time + $transition_time;
        //Calculate total time to use for CSS animation duration later
        $total_time = $frame_time * ($slide_amount);
        //get the percent of time slides and transitions for CSS animation
        //keyframes at a later date
        $transition_percent = $transition_time / $total_time;
        $slide_percent = $slide_time / $total_time;
        //get the amount added to animation_keyframe after each iteration
        $transition_add = ($transition_time / $total_time) * 100;
        $slide_add = ($slide_time / $total_time * 100);
        echo    'Number of slides: ' . $slide_amount . '<br>
                Seconds per slide: ' . $slide_time . '<br>
                Seconds per transition: ' . $transition_time . '<br>
                Total frame time: ' . $frame_time . ' seconds<br>
                Total slider time: ' . $total_time . ' seconds<br>
                Transition frame ratio: ' . $transition_percent . '<br>
                Slide frame ratio: ' . $slide_percent;
        
        $css_output .= '<br>@keyframes ' . $slider_id . ' {
        ';
        //init variables for building the css keyframes
        $frame_position = 0;
        $animation_keyframe = 0;
        $i = 0;
        //Builds out animation keyframes for each slide. 
        while ($i <= $slide_amount) {
            $css_output .= $tab . $animation_keyframe . '&#37 {left: ' . $frame_position . '&#37; }<br>';
            $animation_keyframe += $slide_add;
            //stops transition time on last frame since it is an instant change to give
            //the impression of a carasol.
            if ($i < $slide_amount) {
                $css_output .= $tab . $animation_keyframe . '&#37 {left: ' . $frame_position . '&#37; }<br>';
            }
            $animation_keyframe += $transition_add;
            $frame_position -= 100;
            $i += 1;
        }

        $css_output .= '}
        <br>
        <br>
        #' . $slider_id . ' {<br>
            ' . $tab . 'width: 100%;<br>
            ' . $tab . 'max-width: 1000px;<br>
            ' . $tab . 'margin: 0 auto;<br>
            
        }<br>
        <br>
        #' . $slider_id . ' section {<br>
            ' . $tab . 'overflow: hidden;<br>
        }<br>
        <br>
        #' . $slider_id . ' section figure {<br>
            ' . $tab . 'position: relative;<br>
            ' . $tab . 'width: ' . (($slide_amount + 1) * 100) . '%;<br>
            ' . $tab . 'margin: 0;<br>
            ' . $tab . 'left: 0;<br>
            ' . $tab . 'text-align: left;<br>
            ' . $tab . 'font-size: 0;<br>
            ' . $tab . 'animation: ' . $total_time . 's ' . $slider_id . ' infinite;<br>
        }<br>
        <br>
        #' . $slider_id . ' section figure img {<br>
            ' . $tab . 'width: ' . (100 / ($slide_amount +1)) . '%;<br>
            ' . $tab . 'float: left;<br>
            ' . $tab . 'position: relative;<br>
            ' . $tab . 'z-index: -1;<br>
            ' . $tab . 'margin: 0 auto;<br>
        }';

        $html_output .= ' 
            &lt;div id="' . $slider_id . '"&gt;<br>
            ' . $tab . '&lt;section&gt;<br>
                    ' . $tab . $tab . '&lt;h1&gt;' . $slider_name . '&lt;/h1&gt;<br>
                    ' . $tab . $tab . '&lt;figure&gt;<br>';
                    
            $j = 1;
            $slide_amount +=1;
            while ($j < $slide_amount) {
                $html_output .= '
                        ' . $tab . $tab . $tab . '&lt;img src="img/' . $slider_id . '/slide' . $j . '.jpg" /&gt;<br>
                ';
                $j += 1;
            }
            $html_output .='
                        ' . $tab . $tab . $tab . '&lt;img src="img/' . $slider_id . '/slide1.jpg" /&gt<br>
                    ' . $tab . $tab . '&lt;/figure&gt;<br>
                ' . $tab . '&lt;/section&gt;<br>
            &lt;/div&gt;<br>';
        
        
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <style>
            
            html {
                font-size: 20px;
                font-family: sans-serif;
            }
            
            body {
                width: 100%;
                max-width: 60rem;
                margin: 0 auto;
            }
            
            p {
                width: 100%;
                max-width: 40rem;
                margin: 1rem auto;
            }
            
            h1 {
                font-size: 2rem;
                text-align: center;
            }
            
            form {
                width: 100%;
                max-width: 40rem;
                margin: 2rem auto;
            }
            
            fieldset {
                border: none;
            }
            
            legend span {
                display: inline-block;
                width: 40px;
                height: 40px;
                line-height: 2rem;
                text-align: center;
                margin: .5rem .5rem .5rem -.25rem;
                color: white;
                background-color: cadetblue;
                border-radius: 50%;
            }
            
            input {
                line-height: 2rem;
                border: none;
                background-color: #efefef;
                border-radius: .25rem;
                font-size: 1rem;
                width: 80%;
                margin: .25rem auto;
                padding: 0 1rem;
            }
            
            input[type="number"] {
                width: 6rem;
                padding: 0 1rem 0 2rem;
            }
            
            
            <?php
            echo str_replace('&#37','%',str_replace('&nbsp;',' ',str_replace('&gt;','>',str_replace('&lt;','<',str_replace('<br>',PHP_EOL,$css_output)))));
            ?>
        </style>
    </head>
    <body>
        <h1>CSS Slider Generator</h1>
        <p>
            This tool will generate the HTML and CSS for a simple slider.
            It makes it easy to recalculate the CSS animation which changes
            based on slide ammount. Feel free to use it for any project without
            credit. Please note the sample photos are mine and may not be used else
            where without permission. Happy creating!
        </p>
        <form action="" method="get">
            <h1></h1>
            <fieldset>
                <legend><span class="number">1</span>Give it a name</legend>
                <label for="slider_name">Name: </label><br>
                <input type="text" name="slider_name" /><br>
            </fieldset>
            <fieldset>
                <legend><span class="number">2</span>Add slides and timing</legend>
                <label for="numSlides">Number of slides: </label>
                <input type="number" value="10" min="2" max="30" id="numSlides" name="number_of_slides" /><br>
                <label for="slideTime">Seconds per slide: </label>
                <input type="number" value="6.0" step=".5" min=".5" id="slideTime" name="slide_time" /><br>
                <label for="transitionTime">Transition length?</label>
                <input type="number" value="1.5" step=".5" min=".5" max="10" id="transitionTime" name="transition_time" /><br>
            </fieldset>
            <button type="submit" value="submit" >Generate Slider!</button>
        </form>
        <section id="sample">
        <?php
            if ($html_output != '') {
                echo '<h1>Sample:</h1>';
                echo preg_replace('/img\/.*\/slide/i','img/sample_slides/slide',str_replace('&nbsp;',' ',str_replace('&gt;','>',str_replace('&lt;','<',str_replace('<br>',PHP_EOL,$html_output)))));
            }
        ?>
        </section>
        <code id="output">
            <?php
                if ($css_output != '') {
                    echo '<h1>Output Code: </h1>';
                    echo '<h2>CSS ==></h2>';
                    echo $css_output;
                }
                if ($html_output != '') {
                    echo '<h2>HTML ==></h2>';
                    echo $html_output;
                }
            ?>
        </code>
    </body>
</html>